# Waracle Cake Manager Micro Service
==================================
# A collection of REST APIs for managing adding and viewing "Cake" types.



# Changes
- Converted legacy application to a spring-boot microservice
- Added Front-end (React UI) to allow a user to add/view cake types
- A list of the cake types can now be accessed via the root / context
- Added Continuous Integration - see https://bitbucket.org/mikerek/waracle-cake-manager/addon/pipelines/home#!/results/
- Added Containerisation  of the backend end Microservice (spring boot)
- Detailed changes recorded here: https://bitbucket.org/mikerek/waracle-cake-manager/commits/
_ Test Coverage

## Requirements

For building and running the Springboot application you need:
JDK 1.8
Maven 3

For building and running the front end application (REACT js)
Package manager required: npm/yarn

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute
the main method in the com.waracle.cakemgr.CakeManagerApplication class from your IDE.

Alternatively you can use the Spring Boot Maven plugin like so:

mvn spring-boot:run


Spring boot web server will run at http://localhost:8282/


# REST API Endpoints

Summary
- GET /  Returns details of all cakes in json format
- GET /cake/{id} - Returns the details of a single cake
- GET /cakes - Returns details of all cakes in json format
- POST /cakes - Create a new cake

For more details on API usage see http://localhost:8282/docs/cake-apis.html

# User Interface

The code for the user interface is located in the `cakemgr-frontend-ui` folder.
The interface uses REACT js

To start the front end server
cd into cakemgr-frontend-ui
run: npm start

Access the url http://localhost:3000 to manage cake types




# Containerisation

## Docker
See  [docker](https://www.docker.com/) on examples on how to run image locally
```
E.g.
docker run -p 8282:8282 -t waracle/cake-manager-spring-boot
```
The application will then be accessible at http://localhost:8282/

# todo's
- Containerise the front end UI as well
- Improve test coverage on the UI
- Authentication via OAuth2
