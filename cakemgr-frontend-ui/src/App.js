import React from 'react';
import logo from './logo.svg';
import './App.css';

import {BrowserRouter as Router,  Routes ,Route} from 'react-router-dom'
import HeaderComponent from './components/src/HeaderComponent';
import ListCakeComponent from './components/src/ListCakeComponent';
import CreateCakeComponent from './components/src/CreateCakeComponent';


function App() {
  return (
    <div>
        <Router>
              <HeaderComponent />
                <div className="container">
                    <Routes> 
                         <Route path="/" element={<ListCakeComponent />} />
                         <Route path="/cakes" element={<ListCakeComponent />} />
                         <Route path="/add-cake/:id" element={<CreateCakeComponent />} />
                    </Routes>
                </div>
            
        </Router>
    </div>
    
  );
}

export default App;