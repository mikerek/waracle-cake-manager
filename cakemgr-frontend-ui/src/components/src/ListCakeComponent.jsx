import React, { Component } from 'react'
import CakeService from '../../services/CakeService'
import {withRouter} from '../../services/withRouter'


class ListCakeComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
                cakes: []
        }
        this.addCake = this.addCake.bind(this);
       
    
    }    

    componentDidMount(){
        CakeService.getCakes().then((res) => {
             console.log(res.data)
            this.setState({ cakes: res.data});
        });
    }

    addCake(){
        this.props.navigate('/add-cake/_add')
       
    }

    render() {  

        return (
            <div>
                 <h2 className="text-center">Cake type list</h2>
                 <div className = "row">
                    <button className="btn btn-primary" onClick={this.addCake}> Add Cake</button>
                 </div>
                 <br></br>
                 <div className = "row">
                        <table className = "table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th> Cake title</th>
                                    <th> Cake description</th>
                                    <th> Cake image url</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.cakes.map(
                                        cake => 
                                        <tr key = {cake.id}>
                                             <td> {cake.title} </td>   
                                             <td> {cake.description}</td>
                                             <td> {cake.image}</td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>

                 </div>

            </div>
        )
    }
}
export default withRouter(ListCakeComponent);
