import React, { Component } from 'react'
import CakeService from '../../services/CakeService';
import {withRouter} from '../../services/withRouter';



class CreateCakeComponent extends Component {
    constructor(props) {
        super(props)
      
        this.state = {
            id: this.props.params.id,
            title: '',
            description: '',
            image: ''

        }
         console.log('cake => ' + this.state.id);
        this.changeTitleHandler = this.changeTitleHandler.bind(this);
        this.changeDescriptionHandler = this.changeDescriptionHandler.bind(this);
        this.changeImageHandler = this.changeImageHandler.bind(this);
        this.saveCake = this.saveCake.bind(this);
    }

  
   
     saveCake = (e) => {
        e.preventDefault();
        let cake = {title: this.state.title, description: this.state.description, image: this.state.image};
        console.log('cake => ' + JSON.stringify(cake));
       

         console.log('cake => ' + this.state.id);
       
            CakeService.createCake(cake).then(res =>{
                this.props.navigate('/cakes');
            });
        
    }
    
    changeTitleHandler= (event) => {
        this.setState({title: event.target.value});
    }

    changeDescriptionHandler= (event) => {
        this.setState({description: event.target.value});
    }

    changeImageHandler= (event) => {
        this.setState({image: event.target.value});
    }

    cancel(){
         this.props.navigate('/cakes');
    }

    getTitle(){
        if(this.state.id === '_add'){
            return <h3 className="text-center">Add Cake</h3>
        }
    }
    render() {
        return (
            <div>
                <br></br>
                   <div className = "container">
                        <div className = "row">
                            <div className = "card col-md-6 offset-md-3 offset-md-3">
                                <h3 className="text-center">Add Cake</h3>
                                <div className = "card-body">
                                    <form>
                                        <div className = "form-group">
                                            <label> Title: </label>
                                            <input placeholder="title" name="title" className="form-control" 
                                                value={this.state.title} onChange={this.changeTitleHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Description: </label>
                                            <input placeholder="description" name="description" className="form-control" 
                                                value={this.state.description} onChange={this.changeDescriptionHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Image url: </label>
                                            <input placeholder="image" name="image" className="form-control" 
                                                value={this.state.image} onChange={this.changeImageHandler}/>
                                        </div>

                                        <button className="btn btn-success" onClick={this.saveCake}>Save</button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                   </div>
            </div>
        )
    }
}
export default withRouter(CreateCakeComponent);