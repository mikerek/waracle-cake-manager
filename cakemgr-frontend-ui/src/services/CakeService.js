import axios from 'axios';

const CAKE_API_BASE_URL = "http://localhost:8282/cakes";

class CakeService {

    getCakes(){
        return axios.get(CAKE_API_BASE_URL);
    }

    createCake(cake){
        return axios.post(CAKE_API_BASE_URL, cake);
    }

    getCakeById(cakeId){
        return axios.get(CAKE_API_BASE_URL + '/' + cakeId);
    }

    
}

export default new CakeService()
