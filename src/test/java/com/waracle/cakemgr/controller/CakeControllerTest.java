package com.waracle.cakemgr.controller;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.waracle.cakemgr.domain.Cake;
import com.waracle.cakemgr.domain.Views;
import com.waracle.cakemgr.service.CakeService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;
import java.util.Optional;

import static java.lang.String.format;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
@ExtendWith(SpringExtension.class)
@WebMvcTest(CakeController.class)
@AutoConfigureRestDocs(outputDir = "target/generated-snippets")
public class CakeControllerTest {
    @MockBean
    private CakeService cakeService;
    @Autowired
    private MockMvc mvc;






        @Test
    void whenACakeIsAdded_ItShouldReturnCreatedStatusAndLocationHeaderURI() throws Exception {
        Cake cake = Cake.builder().title("Rum  Cake").id(1L).
                description("Cake made with Rum").image("http://url1").build();

        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(MapperFeature.DEFAULT_VIEW_INCLUSION);

        when(cakeService.addCake(ArgumentMatchers.any(Cake.class))).thenReturn(cake);
        mvc.perform(post("/cakes")
                .content(mapper.writerWithView(Views.Create.class)
                        .writeValueAsString(cake))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(document("cakes/new",
                        requestFields(fieldWithPath("title").description("cake title"),
                                fieldWithPath("description").description("The description of the cake"),
                                fieldWithPath("image").description("The image url "))))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location",
                        containsString("/cakes/" + cake.getId())));


    }




    @Test
    void whenCakesContextPathIsAccessed_shouldListCakesAvailable() throws Exception {
        Cake spongedCake = new Cake(1L, "Sponge Cake", "Yum Yum",
                "http://sponge/icon");
        Cake chocolateCake = new Cake(2L, "Chocolate Cake", "Yum Yum",
                "http://sponge/icon");

        when(cakeService.getCakes()).thenReturn(List.of(spongedCake,chocolateCake));
        mvc.perform(get("/cakes")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[0].title", is("Sponge Cake")))
                .andExpect(jsonPath("$[1].title", is("Chocolate Cake")))
                .andDo(document("cakes/list-cakes by context cake"));
    }

    @Test
    void whenACakeIsSearchedById_shouldReturnMatchingCake() throws Exception {
        Cake spongedCake = new Cake(1L, "Sponge Cake", "Yum Yum",
                "http://sponge/icon");

        when(cakeService.findCake(1L)).thenReturn(Optional.of(spongedCake));
        mvc.perform(get(format("/cake/%d", spongedCake.getId()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.title", is("Sponge Cake")))
                .andDo(document("cakes/get-cake-by-id"));
    }

    @Test
    void whenRootContextPathIsAccessed_shouldListCakesAvailable() throws Exception {
        Cake spongedCake = new Cake(1L, "Sponge Cake", "Yum Yum",
                "http://sponge/icon");
        Cake chocolateCake = new Cake(2L, "Chocolate Cake", "Yum Yum",
                "http://sponge/icon");

        when(cakeService.getCakes()).thenReturn(List.of(spongedCake,chocolateCake));
        mvc.perform(get("/")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[0].title", is("Sponge Cake")))
                .andExpect(jsonPath("$[1].title", is("Chocolate Cake")))
                .andDo(document("cakes/list-cakes by context root"));
    }
}