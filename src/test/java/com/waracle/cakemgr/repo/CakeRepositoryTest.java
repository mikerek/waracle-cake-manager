package com.waracle.cakemgr.repo;


import com.waracle.cakemgr.domain.Cake;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class CakeRepositoryTest {
    @Autowired
    CakeRepository cakeRepository;

    @Test
    public void whenCakeIsSaved_shouldReturnListOfCakes()
    {
        Cake bananaCake = new Cake(12L,"Banana Cake","buttermilk-raspberry butter cake","http://image1" );
        Cake biscuitCake = new Cake(12L,"Biscuit Cake","made of egg whites","http://image1" );
        cakeRepository.saveAll(List.of(bananaCake,biscuitCake));
        Iterable<Cake> cakes = cakeRepository.findAll();
        Assertions.assertThat(cakes).extracting(Cake::getTitle).contains("Banana Cake","Biscuit Cake");
    }

}