package com.waracle.cakemgr.service;


import com.waracle.cakemgr.domain.Cake;
import com.waracle.cakemgr.repo.CakeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import  org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;


@ExtendWith(MockitoExtension.class)
public class CakeServiceTest  {
    @Mock
    CakeRepository cakeRepository;
    @InjectMocks
    CakeService cakeService;
    @Test
    void whenCakeIsAdded_shouldReturnSavedCake(){
      Cake cake =    new Cake(1L,"Sponge Cake",
                "Nice & Sweet","http://image-url");
      when(cakeRepository.save(any(Cake.class))).thenReturn(cake);
      Assertions.assertEquals(cake, cakeService.addCake(cake));
    }

    @Test
    void whenCakesAreAvailableInRepository_shouldReturnAListOfThem(){
        Cake spongeCake =    new Cake(1L,"Sponge Cake",
                "Nice & Sweet","http://image-url");
        Cake bananaCake =    new Cake(1L,"Chocolate Cake",
                "Nice & Sweet","http://image-url");
        when(cakeRepository.findAll()).thenReturn(List.of(spongeCake,bananaCake));
        Iterable<Cake> cakes = cakeService.getCakes();
        org.assertj.core.api.Assertions.assertThat(cakes).extracting(Cake::getTitle).
                contains("Chocolate Cake",
                "Sponge Cake");

    }

    @Test
    void whenACakeIdIsSearched_shouldReturnTheCake(){
        Cake spongeCake =    new Cake(1L,"Sponge Cake",
                "Nice & Sweet","http://image-url");
        when(cakeRepository.findById(1L)).thenReturn(Optional.of(spongeCake));
        Cake foundCake = cakeService.findCake(1L).get();
        Assertions.assertEquals(spongeCake, foundCake);


    }

}