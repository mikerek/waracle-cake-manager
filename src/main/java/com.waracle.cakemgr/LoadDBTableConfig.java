package com.waracle.cakemgr;


import com.waracle.cakemgr.domain.Cake;
import com.waracle.cakemgr.repo.CakeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoadDBTableConfig {

    private static final Logger log = LoggerFactory.getLogger(LoadDBTableConfig.class);

    @Bean
    CommandLineRunner initDatabase(CakeRepository cakeRepository) {
        return args -> {

            log.info("Preloading cake table");
            log.info("Adding  {}", cakeRepository.save(Cake.builder().title("Rum  Cake").
                            description("Cake made with Rum").image("http://url1").build()));
            log.info("Adding  {}", cakeRepository.save(Cake.builder().title("Chocolate  Cake").
                    description("Cake made with Chocolate").image("http://url2").build()));;
        };
    }
}
