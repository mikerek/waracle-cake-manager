package com.waracle.cakemgr.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.waracle.cakemgr.domain.Cake;
import com.waracle.cakemgr.domain.Views;
import com.waracle.cakemgr.service.CakeService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Optional;

import static java.lang.String.format;

@RestController
@AllArgsConstructor
public class CakeController {
    private final CakeService cakeService;

    @PostMapping("/cakes")
    ResponseEntity<Cake> addCake(@RequestBody @JsonView(Views.Create.class) Cake cake) {
        return ResponseEntity.created(URI.create(format("/cakes/%d",
                cakeService.addCake(cake).
                        getId()))).
                build();
    }


    @GetMapping({"/cakes", ""})
    Iterable<Cake> getCakes() {
        return cakeService.getCakes();
    }

    @GetMapping("/cake/{id}")
    Optional<Cake> findCake(@PathVariable Long id) {
        return cakeService.findCake(id);
    }
}
