package com.waracle.cakemgr.service;

import com.waracle.cakemgr.domain.Cake;
import com.waracle.cakemgr.repo.CakeRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@Slf4j
@Service
@AllArgsConstructor
public class CakeService {
    private final CakeRepository cakeRepository;

    public Cake addCake(Cake cake){
       log.info("Adding a cake type to the database");
        return cakeRepository.save(cake);
    }

    public Iterable<Cake> getCakes(){
        log.info("Retrieving the list of cakes from the database");
        return cakeRepository.findAll();
    }

    @GetMapping("/cakes/{id}")
    public Optional<Cake> findCake(@PathVariable Long id) {
        log.info("Retrieving a cake type based on id {} ", id);
        return cakeRepository.findById(id);
    }

}
