package com.waracle.cakemgr.domain;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Cake implements Serializable {

    private static final long serialVersionUID = -1798070786993154676L;



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @JsonView(Views.Create.class)
    @Column(name = "title", unique = true, nullable = false, length = 100)
    private String title;

    @JsonView(Views.Create.class)
    @Column(name = "description", nullable = false, length = 100)
    private String description;

    @JsonView(Views.Create.class)
    @Column(name = "image", nullable = false, length = 300)
    private String image;


}