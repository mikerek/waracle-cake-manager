package com.waracle.cakemgr.repo;

import com.waracle.cakemgr.domain.Cake;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CakeRepository extends CrudRepository<Cake, Long> {
}
